#!/bin/bash
#Initial config

echo 'Start installing the Splunk forwarder ...'
echo 'export SPLUNK_HOME="/opt/splunkforwarder"' >> ~/.bashrc
echo 'export PATH=$PATH:$SPLUNK_HOME/bin' >> ~/.bashrc

source ~/.bashrc

splunk start --accept-license --answer-yes --auto-ports --no-prompt --seed-passwd password

#The forwarder will become a deployment client

splunk set deploy-poll splunk:8089 --accept-license --answer-yes --auto-ports --no-prompt  -auth admin:password

#Configure the splunk forwarder to start at boot time

splunk enable boot-start  

#Configure the Splunk Universal forwarder to forward RASP logs
  #Add the splunk indexer as the forward server and configure inputs for universal forwarder

#splunk add forward-server splunk:9997 -auth admin:password
mkdir -p $SPLUNK_HOME/etc/apps/Prevoty/default

cp /tmp/splunk-forwarder/props.conf $SPLUNK_HOME/etc/apps/Prevoty/default/props.conf
cp /tmp/splunk-forwarder/outputs.conf $SPLUNK_HOME/etc/apps/Prevoty/default/outputs.conf 
cp /tmp/splunk-forwarder/inputs.conf $SPLUNK_HOME/etc/apps/Prevoty/default/inputs.conf

splunk restart
