<%@ page language="java" import="java.sql.*,java.net.*" %>
<%
/**
 * JDBC Connection
 * #creates the "rs" result set and stmt statement for a con connection to a db
 */
 
	//DB Properties
	final String dbIp 	= "mysql";
	final String dbUser 	= "superveda";
	final String dbPassword = "superveda";
	final String dbPort 	= "3306";
	final String dbSid 	= "superveda_db";
	final String dbType 	= "mysql";

	//DB Driver Instantiation
	final String driver_mysql="com.mysql.jdbc.Driver";
	final String driver_mssql="com.microsoft.sqlserver.jdbc.SQLServerDriver";
	if (dbType.compareTo("mysql")==0) {
	    Class.forName(driver_mysql).newInstance();
    } else if (dbType.compareTo("mssql")==0) {
        Class.forName(driver_mssql).newInstance();
    }
	
	Connection con=null;
	ResultSet rs=null;
	Statement stmt=null;	
	
	//Connection String
	String url=null;
	if (dbType.compareTo("mysql")==0) {
	    url="jdbc:mysql://"+ dbIp +":" + dbPort + "/" + dbSid + "?autoReconnect=true";
	} else if (dbType.compareTo("mssql")==0) {
	    url="jdbc:sqlserver://" + dbIp + ":" + dbPort + ";databaseName="+ dbSid +";integratedSecurity=false;";
	}
	con=DriverManager.getConnection(url,dbUser,dbPassword);
	stmt=con.createStatement();
	
	//Query String
	String sql="";
	
	/*
	 * USAGE :
	 * (String)sql >> query string
	 * (ResultSet)rs >> result set >> stms.executeQuery(sql)
	 */
	
%>
