export CATALINA_OPTS="$CATALINA_OPTS -javaagent:/opt/prevoty/prevoty-agent.jar"
export CATALINA_OPTS="$CATALINA_OPTS -Dprevoty.log.config=/opt/prevoty/config/prevoty_logging.json"
export CATALINA_OPTS="$CATALINA_OPTS -Dprevoty.config=/opt/prevoty/config/prevoty_config.json"

